/*!*******************************************************************************************
 *  \file       skill_manager.h
 *  \brief      SkillManager definition file.
 *  \details    This file includes the SkillManager class declaration. To obtain more
 *              information about it's definition consult the skill_manager.cpp file.
 *  \author     David Palacios, Oscar Fernández, Yolanda de la Hoz
 *  \copyright  Copyright 2016 Universidad Politecnica de Madrid (UPM) 
 *
 *     This program is free software: you can redistribute it and/or modify 
 *     it under the terms of the GNU General Public License as published by 
 *     the Free Software Foundation, either version 3 of the License, or 
 *     (at your option) any later version. 
 *   
 *     This program is distributed in the hope that it will be useful, 
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of 
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the 
 *     GNU General Public License for more details. 
 *   
 *     You should have received a copy of the GNU General Public License 
 *     along with this program. If not, see http://www.gnu.org/licenses/. 
 ********************************************************************************/
 
#ifndef SKILL_MANAGER
#define SKILL_MANAGER

#include "drone_process.h"
#include "std_msgs/String.h"
#include "droneMsgsROS/actionData.h"
#include "droneMsgsROS/actionArguments.h"
#include "droneMsgsROS/droneMissionPlannerCommand.h"
#include "droneMsgsROS/SkillDescriptor.h"
#include "droneMsgsROS/SkillsList.h"
#include "droneMsgsROS/SkillState.h"
#include "droneMsgsROS/skillRequest.h"
#include "droneMsgsROS/droneHLCommandAck.h"
#include "droneMsgsROS/droneManagerStatus.h"
#include "droneMsgsROS/dronePose.h"
#include "droneMsgsROS/droneSpeeds.h"
#include "droneMsgsROS/dronePositionRefCommandStamped.h"
//#include "droneMsgsROS/droneSpeedsRefCommand.h"
#include "droneMsgsROS/droneYawRefCommand.h"
#include "droneMsgsROS/setInitDroneYaw_srv_type.h"
#include <geometry_msgs/Vector3Stamped.h>

#include "geometry_msgs/Point.h"
#include <angles/angles.h>

#include <sstream>
//ifstream
#include <fstream>
//XML parser
#include "pugixml.hpp"

//Necessary for setting the odometry after take off
#include "communication_definition.h"
 #include "droneekfstateestimatorinterface.h"




/*!******************************************************************************************
 *  \class      SkillManager
 *  \brief      The SkillManager is the node responsible of translate requested skills and actions.
 *  \details    The SkillManager has the following main functions:
 *
 *               Translate requested actions (e.g., take off, move to a point, etc.) into specific actions for the motor system, i.e., orders for the controllers.
 *
 *               Translate desired active skills (e.g., interpret Aruco visual markers) into running processes.
 *
 *               Guarantee the consistency of requested actions and skills.
 *
 *********************************************************************************************/
class SkillManager : public DroneProcess
{
public:
  //! Constructor. \details The SkillManager constructor doesn't need any argument through argv.
  SkillManager();
  ~SkillManager();
  void readXMLConfig();

  /*!******************************************************************************************************************
   * \brief DroneProcess inherited functions
   *******************************************************************************************************************/ 
  void ownSetUp();
  void ownStart();
  void ownStop();
  void ownRun();

private:
  ros::NodeHandle n;

  pugi::xml_document doc;                                              //!< Attribute storing the reference to the document

   /*!***************************************************************************************
   * \struct skill
   * \brief  This struct store the list of skills (name, state and associated processes)
   ****************************************************************************************/
   struct skill
  {
    droneMsgsROS::SkillDescriptor skill_description;
    std::vector<std::string> process_list;                             //!< Required processes to activate the skill
  };

   /*!***************************************************************************************
   * \struct action
   * \brief  This struct store the list of actions (name and associated processes)
   ****************************************************************************************/
   struct action
  {
    std::string action_name;                                           //!< Name of the action
    std::vector<std::string> process_list;                             //!< Required processes to activate the action
  };

  droneMsgsROS::dronePose   current_drone_position_reference;
  droneMsgsROS::droneSpeeds current_drone_speed_reference;

  droneMsgsROS::dronePose last_pose;
  droneMsgsROS::actionData current_action;
  droneMsgsROS::actionData previous_action;
  droneMsgsROS::droneManagerStatus last_manager_status;

  geometry_msgs::Vector3Stamped last_rotation_angles_msg;

  std::string requested_action;                                        //!< Attribute storing topic name to request action from MissionPlannerScheduler and HMI.
  std::string droneMissionHLCommandAck;                                //!< Attribute storing topic name to receive the acknowledge from ManagerOfActions.
  std::string droneManagerStatus;                                      //!< Attribute storing topic name to receive the current state from ManagerOfActions.
  std::string approved_action;                                         //!< Attribute storing topic name to publish the action that has been approved and send to the ManagerOfActions.
  std::string skills_list_topic;                                    //!< Attribute storing topic name to publish the requested skills.
  std::string droneMissionPlannerCommand;                              //!< Attribute storing topic name to publish commands to the ManagerOfActions.
  std::string dronePositionRefs;                                       //!< Attribute storing topic name to publish position reference commands to the motor system.
  std::string pointToLook;                                             //!< Attribute storing topic name to publish the point to look to the yawCommander
  std::string yawToLook;                                               //!< Attribute storing topic name to publish the yaw to look to the yawCommander
  std::string destination_point;                                       //!< Attribute storing topic name to publish the destination point.
  std::string dronePose;                                               //!< Attribute storing topic name to receive the current pose.


  std::string action_model_file;
  std::string drone_id;
  std::string drone_id_namespace;
  std::string my_stack_directory;
  std::string skill_manager_process_dir;
  std::string action_model_dir;  


  bool action_ack;                                                     //!< Attribute storing topic name to receive alive messages from DroneProcess.
  bool moving_rel;
  bool moving_abs;
  bool rotation;

  std::vector<skill> skills_list;                                      //!< Attribute storing the skill list
  std::vector<action> actions_list;                                    //!< Attribute storing the action list


  std::vector<std::string> drone_modules;                              //!< Attribute storing the modules to be sent to the ManagerOfActions.
  
  ros::ServiceServer skill_request_srv;                                //!< ROS service handler used to request skills.

  ros::ServiceClient setInitDroneYaw_srv_server;

  ros::Subscriber action_request_sub;                                  //!< ROS subscriber handler used to receives the request action from the HMI or MissionPlannerScheduler.
  ros::Subscriber command_ack_sub;                                     //!< ROS subscriber handler used to receives the acknowledge from the ManagerOfActions.
  ros::Subscriber current_pos_sub;                                     //!< ROS subscriber handler used to receives the current pose.
  ros::Subscriber drone_rotation_angles_sub;

  ros::Publisher approved_action_pub;                                  //!< ROS publisher handler used to send the approved action to the HMI and actionMonitor.
  ros::Publisher skills_list_pub;                                      //!< ROS publisher handler used to send the state of the skills to the HMI.
  ros::Publisher command_pub;                                          //!< ROS publisher handler used to send commands to the ManagerOfActions.
  ros::Publisher speeds_refs_pub;                                      //!< ROS publisher handler used to send speed reference commands to the motor system.
  ros::Publisher point_to_look_pub;                                    //!< ROS publisher handler used to send the point to look to the yawCommander.
  ros::Publisher yaw_to_look_pub;                                      //!< ROS publisher handler used to send the yaw to look to the yawCommander.
  ros::Publisher destination_point_pub;                                //!< ROS publisher handler used to send the destination point.

  bool parseXMLConfig(std::string modelOfSkillsConfigFile);


  void sendSkillsList();
  bool getSkill(std::string name, skill** current_skill);
  void initSkillsList();
  bool getAction(std::string name, action** current_action);
  void initActionsList();
  std::vector<std::string> translateSkillsIntoProcess();
  std::vector<std::string> translateActionIntoProcesses(std::string name);
  void printModulesNames(std::vector<std::string> drone_modules);

  int32_t last_action_request;

  /*!***************************************************************************************
   * \details Check the consistency of the requested skill
   * \param skill_name The skill name
   * \return bool function
   ****************************************************************************************/
  bool checkConsistency();

  /*!***************************************************************************************
   * \details This is the method called every time a new message arrives through the 
   *  'requested_action' topic.
   * \param msg The recieved message that needs to be processed.
   * \return Void function
   ****************************************************************************************/
  void actionRequestCallback(const droneMsgsROS::actionData::ConstPtr& msg);

   /*!***************************************************************************************
   * \details This is the method called every time a new message arrives through the 
   *  'droneMissionHLCommandAck' topic.
   * \param msg The recieved message that needs to be processed.
   * \return Void function
   ****************************************************************************************/
  void commandAckCallback(const droneMsgsROS::droneHLCommandAck &msg);

  /*!***************************************************************************************
   * \details Service for request skills
   * \return A Boolean indicating if the service has run correctly
   ****************************************************************************************/
  bool skillRequestCall(droneMsgsROS::skillRequest::Request &request, droneMsgsROS::skillRequest::Response &response);

  /*!***************************************************************************************
   * \details This is the method called every time a new message arrives through the 
   *  'EstimatedPose_droneGMR_wrt_GFF' topic.
   * \param msg The recieved message that needs to be processed.
   * \return Void function
   ****************************************************************************************/
  void currentPositionCallback(const droneMsgsROS::dronePose &msg);

  /*!***************************************************************************************
   * \details This is the method checks wheter a actionData msg is well defined. If the message
   * is not well defined, this method output a message which specifies the problem.
   * \param msg The recieved message that needs to be checked.
   * \return A boolean indicating if the message is well defined.
   ****************************************************************************************/
  bool checkArguments(const droneMsgsROS::actionData& msg);

  /*!***************************************************************************************
   * \details This is the method checks wheter a actionArguments msg is well defined. If the message
   * is not well defined, this method output a message which specifies the problem.
   * \param msg The recieved message that needs to be checked.
   * \return A boolean indicating if the message is well defined.
   ****************************************************************************************/
  bool checkArgumentValue(const droneMsgsROS::actionArguments& msg);

  void droneRotationAnglesCallback(const geometry_msgs::Vector3Stamped& msg);

  void configureOdometryStateEstimator();
};

#endif
