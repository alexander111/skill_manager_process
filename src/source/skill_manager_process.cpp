#include "../include/skill_manager_process.h"
#include <iostream>

using namespace std;

SkillManager::SkillManager():DroneProcess()
{
  printf("%s\n", "Starting skill_manager_process");

  moving_rel=false;
  moving_abs=false;
  rotation=false;

  DroneEKFStateEstimatorInterface drone_ekf_state_estimator_interface(MODULE_NAME_ODOMETRY_STATE_ESTIMATOR, ModuleNames::ODOMETRY_STATE_ESTIMATOR);
  drone_ekf_state_estimator_interface.open(n);
}

SkillManager::~SkillManager()
{
  printf("%s\n", "Closing skill_manager_process");
}

void SkillManager::ownSetUp()
{
  printf("%s\n", "Setting up skill_manager_process");

  //Get params
  n.param<string>("requested_action", requested_action, "requested_action");
  n.param<string>("droneMissionHLCommandAck", droneMissionHLCommandAck, "droneMissionHLCommandAck");
  n.param<string>("approved_action", approved_action, "approved_action");
  n.param<string>("skills_list", skills_list_topic, "skills_list");
  n.param<string>("droneMissionPlannerCommand", droneMissionPlannerCommand, "droneMissionPlannerCommand");
  n.param<string>("destination_point", destination_point, "droneMissionPoint");
  n.param<string>("pointToLook", pointToLook, "pointToLook");
  n.param<string>("droneYawToLook", yawToLook, "droneYawToLook");
  n.param<string>("dronePose", dronePose, "EstimatedPose_droneGMR_wrt_GFF");

  // Parameters config file
  n.param<string>("action_model_file", action_model_file, "action_model.xml");
  n.param<string>("my_stack_directory", my_stack_directory, "~/workspace/ros/quadrotor_stack_catkin/src/quadrotor_stack");
  n.param<string>("drone_id", drone_id, "1");
  n.param<string>("drone_id_namespace", drone_id_namespace, "drone"+drone_id);
  n.param<string>("skill_manager_process_dir", skill_manager_process_dir, my_stack_directory+"/stack_devel/executive_system/skill_manager/skill_manager_process");
  n.param<string>("action_model_dir", action_model_dir, "");

  if (action_model_dir.empty())
  {
    action_model_dir=my_stack_directory+"/configs/"+drone_id_namespace;;
  }

  readXMLConfig();
}

void SkillManager::ownStart()
{
  // connect services
  skill_request_srv=n.advertiseService("request_skill",&SkillManager::skillRequestCall,this);
  setInitDroneYaw_srv_server=n.serviceClient<droneMsgsROS::setInitDroneYaw_srv_type>("droneOdometryStateEstimator/setInitDroneYaw");


  // connect subscribers
  action_request_sub = n.subscribe(requested_action, 1000, &SkillManager::actionRequestCallback, this);
  command_ack_sub = n.subscribe(droneMissionHLCommandAck, 1000, &SkillManager::commandAckCallback, this);
  current_pos_sub=n.subscribe(dronePose, 1000, &SkillManager::currentPositionCallback, this);
  drone_rotation_angles_sub = n.subscribe("rotation_angles", 1, &SkillManager::droneRotationAnglesCallback, this);

  // connect publishers
  approved_action_pub = n.advertise<droneMsgsROS::actionData>(approved_action, 10);
  skills_list_pub = n.advertise<droneMsgsROS::SkillsList>(skills_list_topic, 10,true);
  command_pub = n.advertise<droneMsgsROS::droneMissionPlannerCommand>(droneMissionPlannerCommand, 10);
  point_to_look_pub = n.advertise<droneMsgsROS::dronePositionRefCommandStamped>(pointToLook, 10);//TODO
  yaw_to_look_pub = n.advertise<droneMsgsROS::droneYawRefCommand>(yawToLook, 1, true);
  destination_point_pub=n.advertise<droneMsgsROS::dronePositionRefCommand>(destination_point, 1, true);

  sendSkillsList();
}

void SkillManager::ownStop()
{
  // shutdown services
  skill_request_srv.shutdown();
  setInitDroneYaw_srv_server.shutdown();

  // shutdown subscribers
  action_request_sub.shutdown();
  command_ack_sub.shutdown();
  current_pos_sub.shutdown();
  drone_rotation_angles_sub.shutdown();

  // shutdown publishers
  approved_action_pub.shutdown();
  skills_list_pub.shutdown();
  command_pub.shutdown();
  point_to_look_pub.shutdown();
  yaw_to_look_pub.shutdown();
  destination_point_pub.shutdown();
}

void SkillManager::ownRun()
{
  //sendSkillsList();
}

void SkillManager::printModulesNames(vector<string> drone_modules)
{
  cout << "----LIST OF MODULES TO BE SEND----" << endl;
  vector<string>::iterator iterador = drone_modules.begin();
  while(iterador != drone_modules.end())
  {
    cout << *iterador << endl;
    iterador++;
  }
}


bool SkillManager::getSkill(string name, skill** current_skill)
{
  vector<skill>::iterator iterador = skills_list.begin();
  while(iterador != skills_list.end())
  {
    if(name.compare(iterador->skill_description.name)==0)
    {
      *current_skill = &(*iterador);
      return true;
    }
    iterador++;
  }
  return false;
}

vector<string> SkillManager::translateSkillsIntoProcess()
{
  vector<string> processes_to_activate;
  vector<skill>::iterator iterador = skills_list.begin();
  while(iterador != skills_list.end())
  {
    if(iterador->skill_description.current_state.state==droneMsgsROS::SkillState::requested)
    {
      cout << iterador->skill_description.name << endl;
      processes_to_activate.insert(end(processes_to_activate), begin(iterador->process_list), end(iterador->process_list));
    }
    iterador++;
  }
  return processes_to_activate;
}

// send the skill list to the HMI
void SkillManager::sendSkillsList()
{
  droneMsgsROS::SkillsList skill_list_msg;
  droneMsgsROS::SkillDescriptor skill_descriptor;
  for (int idx = 0; idx < skills_list.size(); idx++)
  {
    skill_list_msg.skill_list.push_back(skills_list[idx].skill_description);
  }
  skills_list_pub.publish(skill_list_msg);
}

vector<string> SkillManager::translateActionIntoProcesses(string name)
{
  vector<string> processes_to_activate;
  vector<action>::iterator iterador = actions_list.begin();
  while(iterador != actions_list.end())
  {
    if(name.compare(iterador->action_name)==0)
      processes_to_activate.insert(end(processes_to_activate), begin(iterador->process_list), end(iterador->process_list));

    iterador++;
  }
  return processes_to_activate;
}

void SkillManager::initActionsList()
{
  cout << "\033[1;33m............INIT ACTIONS............\033[0m" << endl;
  for (pugi::xml_node root = doc.child("action_model").child("action"); root; root = root.next_sibling("action"))
  {
    cout << "\033[1;34m...Action: \033[0m" << root.attribute("name").as_string() << endl;
    action new_action;
    new_action.action_name=root.attribute("name").as_string();

    for (pugi::xml_node tool = root.child("process"); tool; tool = tool.next_sibling("process"))
    {
      cout << "\033[1;34m........Process: \033[0m" << "\033[1;32m" << tool.attribute("name").as_string() <<"\033[0m"<< endl;
      new_action.process_list.push_back(tool.attribute("name").as_string());
    }
    actions_list.push_back(new_action);
  }
  return;
}

void SkillManager::initSkillsList()
{
  cout << "\033[1;33m............INIT SKILLS............\033[0m" << endl;
  for (pugi::xml_node root = doc.child("action_model").child("skill"); root; root = root.next_sibling("skill"))
  {
    cout << "\033[1;34m...Skill: \033[0m" << root.attribute("name").as_string() << endl;
    skill new_skill;
    new_skill.skill_description.name=root.attribute("name").as_string();
    new_skill.skill_description.current_state.state=droneMsgsROS::SkillState::not_requested;

    for (pugi::xml_node tool = root.child("process"); tool; tool = tool.next_sibling("process"))
    {
      cout << "\033[1;34m........Process: \033[0m" << "\033[1;32m" << tool.attribute("name").as_string() <<"\033[0m"<< endl;
      new_skill.process_list.push_back(tool.attribute("name").as_string());
    }
    skills_list.push_back(new_skill);
  }
  return;
}

bool SkillManager::parseXMLConfig(string modelOfSkillsConfigFile)
{
  // xml parser
  ifstream nameFile(modelOfSkillsConfigFile.c_str());
  pugi::xml_parse_result result = doc.load(nameFile);

  if(!result){
    cout << "XML [" << modelOfSkillsConfigFile << "] \n";
    cout << "Error description: " << result.description() << "\n";
    cout << "Error offset: " << result.offset << " (error at [..." << (modelOfSkillsConfigFile.c_str() + result.offset) << "]\n\n";
    return false;
  }
  else
    return true;
}

void SkillManager::readXMLConfig()
{
  if(parseXMLConfig(action_model_dir+ "/" + action_model_file)){
    initSkillsList();
    initActionsList();
  }
  return;
}

bool SkillManager::skillRequestCall(droneMsgsROS::skillRequest::Request &request, droneMsgsROS::skillRequest::Response &response)
{
  skill* current_skill;
  if(request.activate)//Always true
   {
     if(checkConsistency()){
       cout << "Saving request skill in skills_list: " + request.skill_name << endl;
       if (getSkill(request.skill_name.c_str(),&current_skill)){
           current_skill->skill_description.current_state.state=droneMsgsROS::SkillState::requested;
           response.ack = true;
       }else{
         response.ack = false;
         response.why = "Incorrect skill";
       }
     }else{ 
       response.ack = false;
       response.why = "Inconsistent skill";
     }
   }
  sendSkillsList(); // send the skill list to the HMI
  return true;
}

void SkillManager::actionRequestCallback(const droneMsgsROS::actionData::ConstPtr& msg)
{
  int32_t last_action_request = msg->mpAction;
  
  if (!checkArguments(*msg))
  {
    cout << "\33[31mAction Arguments not valid, Action not approved\33[0m"<< endl;
    droneMsgsROS::actionData approved_action_msg;
    approved_action_msg.ack       = false;
    approved_action_msg.mpAction  = msg->mpAction;
  
    approved_action_msg.arguments = msg->arguments;
    approved_action_pub.publish(approved_action_msg);
    return;
  }

  if(previous_action.mpAction==droneMsgsROS::actionData::TAKE_OFF)
  {
    configureOdometryStateEstimator();
  }
  
  string action;

  droneMsgsROS::droneMissionPlannerCommand drone_command_msg;

  switch(last_action_request)
  {
    case droneMsgsROS::actionData::TAKE_OFF:
      cout << "\33[32mReceived Take-Off action request\33[0m" << endl;
      drone_command_msg.mpCommand = droneMsgsROS::droneMissionPlannerCommand::TAKE_OFF;
      action="TAKE_OFF";
      current_action=*msg;
      break;
    case droneMsgsROS::actionData::HOVER:
      cout << "\33[32mReceived Hover action request\33[0m" << endl;
      drone_command_msg.mpCommand = droneMsgsROS::droneMissionPlannerCommand::HOVER;
      action="HOVER";
      current_action=*msg;
      break;
    case droneMsgsROS::actionData::STABILIZE:
      cout << "\33[32mReceived Stabilize action request\33[0m" << endl;
      drone_command_msg.mpCommand = droneMsgsROS::droneMissionPlannerCommand::HOVER;
      action="HOVER";
      current_action=*msg;
      break;
    case droneMsgsROS::actionData::MOVE:
      cout << "\33[32mReceived Move action request\33[0m" << endl;
      //drone_command_msg.mpCommand = droneMsgsROS::droneMissionPlannerCommand::MOVE_POSITION;
      drone_command_msg.mpCommand = droneMsgsROS::droneMissionPlannerCommand::MOVE_TRAJECTORY;
      action="MOVE_POSITION";
      moving_rel=true;
      current_action=*msg;
      break;
    case droneMsgsROS::actionData::GO_TO_POINT:
      cout << "\33[32mReceived Go to point action request\33[0m" << endl;
      drone_command_msg.mpCommand = droneMsgsROS::droneMissionPlannerCommand::MOVE_TRAJECTORY;
      action="MOVE_POSITION";
      moving_abs=true;
      current_action=*msg;
      break;
    case droneMsgsROS::actionData::ROTATE_YAW:
      cout << "\33[32mReceived rotate yaw action request\33[0m" << endl;
      drone_command_msg.mpCommand = droneMsgsROS::droneMissionPlannerCommand::MOVE_TRAJECTORY;
      action="MOVE_POSITION";
      rotation=true;
      current_action=*msg;
      break;
    case droneMsgsROS::actionData::FLIP_RIGHT:
      cout << "\33[32mReceived Flip-Right action request\33[0m" << endl;
      drone_command_msg.mpCommand = droneMsgsROS::droneMissionPlannerCommand::MOVE_FLIP_RIGHT;
      action="MOVE_FLIP_RIGHT";
      current_action=*msg;
      break;
    case droneMsgsROS::actionData::FLIP_LEFT:
      cout << "\33[32mReceived Flip-Left action request\33[0m" << endl;
      drone_command_msg.mpCommand = droneMsgsROS::droneMissionPlannerCommand::MOVE_FLIP_LEFT;
      action="MOVE_FLIP_LEFT";
      current_action=*msg;
      break;
    case droneMsgsROS::actionData::FLIP_FRONT:
      cout << "\33[32mReceived Flip-Front action request\33[0m" << endl;
      drone_command_msg.mpCommand = droneMsgsROS::droneMissionPlannerCommand::MOVE_FLIP_FRONT;
      action="MOVE_FLIP_FRONT";
      current_action=*msg;
      break;
    case droneMsgsROS::actionData::FLIP_BACK:
      cout << "\33[32mReceived Flip-Back action request\33[0m" << endl;
      drone_command_msg.mpCommand = droneMsgsROS::droneMissionPlannerCommand::MOVE_FLIP_BACK;
      action="MOVE_FLIP_BACK";
      current_action=*msg;
      break;
    case droneMsgsROS::actionData::LAND:
      cout << "\33[32mReceived Land action request\33[0m" << endl;
      drone_command_msg.mpCommand = droneMsgsROS::droneMissionPlannerCommand::LAND;
      action="LAND";
      current_action=*msg;
      break;
    default:
      cout << "\33[31mBad request, value: "<< last_action_request << " is not accepted\33[0m" << endl;
      break;
  }

  // get the processes associated to the action and append the drone_modules
  cout << "translating action into modules" << endl;
  vector<string> drone_modules; // It stores the modules to be sent to the ManagerOfActions.
  drone_modules=translateActionIntoProcesses(action);

  // get the processes associated to the skill and append the drone_modules
  sendSkillsList();
  cout << "translating skills into modules" << endl;
  vector<string> skill_modules = translateSkillsIntoProcess();
  drone_modules.insert(end(drone_modules), begin(skill_modules), end(skill_modules));

  printModulesNames(drone_modules);
  drone_command_msg.drone_modules_names =drone_modules;
  command_pub.publish(drone_command_msg);
}

bool SkillManager::checkConsistency()
{
  return true;
}

void SkillManager::commandAckCallback(const droneMsgsROS::droneHLCommandAck &msg)
{
  cout << "\33[34mSending approved_action...\33[0m" << endl;
  droneMsgsROS::actionData approved_action_msg;
  approved_action_msg.ack       = msg.ack;
  approved_action_msg.mpAction  = current_action.mpAction;
  
  approved_action_msg.arguments = current_action.arguments;

  // publish approved action 
  approved_action_pub.publish(approved_action_msg);

  cout << "Erasing skills from skills_list" << endl;
  sendSkillsList();
  vector<skill>::iterator iterador = skills_list.begin();
  while(iterador != skills_list.end())
  {
    iterador->skill_description.current_state.state=droneMsgsROS::SkillState::not_requested;
    iterador++;
  }

  if (moving_rel || moving_abs)
  {
    cout << "Sending final position" << endl;

    droneMsgsROS::dronePositionRefCommand final_point;

    cout << "\33[35mActual Pose\nx=" << last_pose.x << "\ny=" << last_pose.y << "\nz=" << last_pose.z <<"\33[0m" << endl;

    geometry_msgs::Point argument_point;

    argument_point.x=current_action.arguments.at(0).value.at(0);
    argument_point.y=current_action.arguments.at(0).value.at(1);
    argument_point.z=current_action.arguments.at(0).value.at(2);
    

    if ( moving_rel )
    {
      final_point.x=argument_point.x+last_pose.x;
      final_point.y=argument_point.y+last_pose.y;
      final_point.z=argument_point.z+last_pose.z;
    }
    else if ( moving_abs )
    {
      final_point.x=argument_point.x;
      final_point.y=argument_point.y;
      final_point.z=argument_point.z;
    }

    cout << "\33[32mFinal Point\nx=" << final_point.x << "\ny=" << final_point.y << "\nz=" << final_point.z <<"\33[0m" << endl;

    destination_point_pub.publish(final_point);

    moving_rel=false;
    moving_abs=false;
  }

  else if ( rotation )
  {
    cout << "Sending final yaw to look" << endl;

    droneMsgsROS::dronePositionRefCommand final_point;
    final_point.x=last_pose.x;
    final_point.y=last_pose.y;
    final_point.z=last_pose.z;

    droneMsgsROS::droneYawRefCommand yawMsg;
    yawMsg.header.stamp=ros::Time::now();

    cout << "\33[35mActual Yaw: " << last_pose.yaw << "\33[0m" << endl;
    cout << "\33[32mFinal Yaw: " << current_action.arguments.at(0).value.at(0) << "\33[0m" << endl;
    
    yawMsg.yaw=angles::from_degrees(current_action.arguments.at(0).value.at(0));//RADIANS
    yaw_to_look_pub.publish(yawMsg);

    rotation=false;
  }
  previous_action=current_action;

}

void SkillManager::currentPositionCallback(const droneMsgsROS::dronePose &msg)
{
  last_pose=msg;
}

bool SkillManager::checkArguments(const droneMsgsROS::actionData& msg)
{
  bool result=true;
  int32_t last_action_request = msg.mpAction;
  string action_name;
  switch(last_action_request)
  {
    case droneMsgsROS::actionData::TAKE_OFF:
      if (msg.arguments.size()!=0)
      {
        cout << "\33[31;1mERROR: Take off has received an unexpected number of arguments\nExpected: 0\nReceived: "<< msg.arguments.size() <<"\33[0m" << endl;
        result=false;
      }
      break;
    case droneMsgsROS::actionData::HOVER:
      if (msg.arguments.size()!=1)
      {
        cout << "\33[31;1mERROR: Hover has received an unexpected number of arguments\nExpected: 1\nReceived: "<< msg.arguments.size() <<"\33[0m" << endl;
        result=false;
      }
      else if(msg.arguments.at(0).argumentName!=droneMsgsROS::actionArguments::DURATION)
      {
        stringstream arg_name;
        switch(msg.arguments.at(0).argumentName)
        {
          case droneMsgsROS::actionArguments::DURATION:
            arg_name << "DURATION";
            break;
          case droneMsgsROS::actionArguments::DESTINATION:
            arg_name << "DESTINATION";
            break;
          case droneMsgsROS::actionArguments::ROTATION:
            arg_name << "ROTATION";
            break;
          default:
            arg_name << "nonexistent value \"" << msg.arguments.at(0).argumentName << "\"";
            break;
        }
        cout << "\33[31;1mERROR: The argument received didn't match the argument expected\nExpected: DURATION\nReceived: "<< arg_name <<"\33[0m" << endl;
        result=false;
      }
      else
        result=checkArgumentValue(msg.arguments.at(0));
      break;
    case droneMsgsROS::actionData::STABILIZE:
      if (msg.arguments.size()!=0)
      {
        cout << "\33[31;1mERROR: Stabilize has received an unexpected number of arguments\nExpected: 0\nReceived: "<< msg.arguments.size() <<"\33[0m" << endl;
        result=false;
      }
      break;
    case droneMsgsROS::actionData::MOVE:
      if (msg.arguments.size()!=1)
      {
        cout << "\33[31;1mERROR: Move has received an unexpected number of arguments\nExpected: 1\nReceived: "<< msg.arguments.size() <<"\33[0m" << endl;
        result=false;
      }
      else if(msg.arguments.at(0).argumentName!=droneMsgsROS::actionArguments::DESTINATION)
      {
        stringstream arg_name;
        switch(msg.arguments.at(0).argumentName)
        {
          case droneMsgsROS::actionArguments::DURATION:
            arg_name << "DURATION";
            break;
          case droneMsgsROS::actionArguments::DESTINATION:
            arg_name << "DESTINATION";
            break;
          case droneMsgsROS::actionArguments::ROTATION:
            arg_name << "ROTATION";
            break;
          default:
            arg_name << "nonexistent value \"" << msg.arguments.at(0).argumentName << "\"";
            break;
        }
        cout << "\33[31;1mERROR: The argument received didn't match the argument expected\nExpected: DESTINATION\nReceived: "<< arg_name <<"\33[0m" << endl;
        result=false;
      }
      else
        result=checkArgumentValue(msg.arguments.at(0));
      break;
    case droneMsgsROS::actionData::GO_TO_POINT:
      if (msg.arguments.size()!=1)
      {
        cout << "\33[31;1mERROR: Go to point has received an unexpected number of arguments\nExpected: 1\nReceived: "<< msg.arguments.size() <<"\33[0m" << endl;
        result=false;
      }
      else if(msg.arguments.at(0).argumentName!=droneMsgsROS::actionArguments::DESTINATION)
      {
        stringstream arg_name;
        switch(msg.arguments.at(0).argumentName)
        {
          case droneMsgsROS::actionArguments::DURATION:
            arg_name << "DURATION";
            break;
          case droneMsgsROS::actionArguments::DESTINATION:
            arg_name << "DESTINATION";
            break;
          case droneMsgsROS::actionArguments::ROTATION:
            arg_name << "ROTATION";
            break;
          default:
            arg_name << "nonexistent value \"" << msg.arguments.at(0).argumentName << "\"";
            break;
        }
        cout << "\33[31;1mERROR: The argument received didn't match the argument expected\nExpected: DESTINATION\nReceived: "<< arg_name <<"\33[0m" << endl;
        result=false;
      }
      else
        result=checkArgumentValue(msg.arguments.at(0));
      break;
    case droneMsgsROS::actionData::ROTATE_YAW:
      if (msg.arguments.size()!=1)
      {
        cout << "\33[31;1mERROR: Rotate yaw has received an unexpected number of arguments\nExpected: 1\nReceived: "<< msg.arguments.size() <<"\33[0m" << endl;
        result=false;
      }
      else if(msg.arguments.at(0).argumentName!=droneMsgsROS::actionArguments::ROTATION)
      {
        stringstream arg_name;
        switch(msg.arguments.at(0).argumentName)
        {
          case droneMsgsROS::actionArguments::DURATION:
            arg_name << "DURATION";
            break;
          case droneMsgsROS::actionArguments::DESTINATION:
            arg_name << "DESTINATION";
            break;
          case droneMsgsROS::actionArguments::ROTATION:
            arg_name << "ROTATION";
            break;
          default:
            arg_name << "nonexistent value \"" << msg.arguments.at(0).argumentName << "\"";
            break;
        }
        cout << "\33[31;1mERROR: The argument received didn't match the argument expected\nExpected: ROTATION\nReceived: "<< arg_name <<"\33[0m" << endl;
        result=false;
      }
      else
        result=checkArgumentValue(msg.arguments.at(0));
      break;
    case droneMsgsROS::actionData::FLIP_RIGHT:
      if (msg.arguments.size()!=0)
      {
        cout << "\33[31;1mERROR: Flip right has received an unexpected number of arguments\nExpected: 0\nReceived: "<< msg.arguments.size() <<"\33[0m" << endl;
        result=false;
      }
      break;
    case droneMsgsROS::actionData::FLIP_LEFT:
      if (msg.arguments.size()!=0)
      {
        cout << "\33[31;1mERROR: Flip left has received an unexpected number of arguments\nExpected: 0\nReceived: "<< msg.arguments.size() <<"\33[0m" << endl;
        result=false;
      }
      break;
    case droneMsgsROS::actionData::FLIP_FRONT:
      if (msg.arguments.size()!=0)
      {
        cout << "\33[31;1mERROR: Flip front has received an unexpected number of arguments\nExpected: 0\nReceived: "<< msg.arguments.size() <<"\33[0m" << endl;
        result=false;
      }
      break;
    case droneMsgsROS::actionData::FLIP_BACK:
      if (msg.arguments.size()!=0)
      {
        cout << "\33[31;1mERROR: Flip back has received an unexpected number of arguments\nExpected: 0\nReceived: "<< msg.arguments.size() <<"\33[0m" << endl;
        result=false;
      }
      break;
    case droneMsgsROS::actionData::LAND:
      if (msg.arguments.size()!=0)
      {
        cout << "\33[31;1mERROR: Land has received an unexpected number of arguments\nExpected: 0\nReceived: "<< msg.arguments.size() <<"\33[0m" << endl;
        result=false;
      }
      break;
    default:
      cout << "\33[31mERROR: Bad request, value: "<< last_action_request << " is not accepted\33[0m" << endl;
      break;
  }
  return result;
}

bool SkillManager::checkArgumentValue(const droneMsgsROS::actionArguments& argument)
{
  bool result=true;
  int32_t argument_name = argument.argumentName;
  string action_name;
  switch(argument_name)
  {
    case droneMsgsROS::actionArguments::DURATION:
      if(argument.value.size()!=1)
      {
        cout << "\33[31;1mERROR: Argument Duration has received an unexpected number of values\nExpected: 1\nReceived: "<< argument.value.size() <<"\33[0m" << endl;
        result=false;
      }
      break;
    case droneMsgsROS::actionArguments::DESTINATION:
      if(argument.value.size()!=3)
      {
        cout << "\33[31;1mERROR: Argument Destination has received an unexpected number of values\nExpected: 3\nReceived: "<< argument.value.size() <<"\33[0m" << endl;
        result=false;
      }
      break;
    case droneMsgsROS::actionArguments::ROTATION:
      if(argument.value.size()!=1)
      {
        cout << "\33[31;1mERROR: Argument Rotation has received an unexpected number of values\nExpected: 1\nReceived: "<< argument.value.size() <<"\33[0m" << endl;
        result=false;
      }
      break;
    default:
      cout << "\33[31;1mERROR: nonexistent argument\33[0m" << endl;
      result=false;
      break;
  }
  return result;
}

void SkillManager::droneRotationAnglesCallback(const geometry_msgs::Vector3Stamped& msg)
{
  last_rotation_angles_msg = (msg);
}

void SkillManager::configureOdometryStateEstimator()
{
  droneMsgsROS::setInitDroneYaw_srv_type setInitDroneYaw_srv_var;
  setInitDroneYaw_srv_var.request.yaw_droneLMrT_telemetry_rad = (last_rotation_angles_msg.vector.z)*(M_PI/180.0);
  setInitDroneYaw_srv_server.call(setInitDroneYaw_srv_var);
  cout << "\033[1;31mSe le manda el yaw al odometry, con valor: " << setInitDroneYaw_srv_var.request.yaw_droneLMrT_telemetry_rad << "\033[0m" << endl;
}